var lastTime = -400;
var name_now;
var final_score = 0;
var rank1_name = 'NULL',rank1_score = 0;
var rank2_name = 'NULL',rank2_score = 0;
var rank3_name = 'NULL',rank3_score = 0;
var rank4_name = 'NULL',rank4_score = 0;
var rank5_name = 'NULL',rank5_score = 0;
var floortimeInterval = 1000;
var springSound,damageSound,dieSound;

var menu = {
    preload: function(){
        game.load.image('bg', 'assets/bg.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('play_button', 'assets/play_button.png');
        game.load.image('rank_button', 'assets/rank_button.png');
        game.load.image('menu', 'assets/menu.png');
        game.load.image('title', 'assets/title.png');
        game.load.audio('bg_music','assets/bg_music.wav');
    },

    create: function(){
        game.add.image(0, 0, 'bg');
        game.add.image(0, 0, 'ceiling');
        game.add.image(400, 0, 'ceiling');
        game.add.image(120, 200, 'title');

        this.button = game.add.button(160,450,'play_button',PlayOnClick,this,2,1,0);
        this.button = game.add.button(160,600,'rank_button',RankOnClick,this,2,1,0);
    }
}
function PlayOnClick(){
    game.state.start('nameSetting');
}
function RankOnClick(){
    game.state.start('rank');
    var num = 0;
    var scoredata = {
        name:[],
        score:[]
    };
    firebase.database().ref('rank/').orderByChild("score").once('value',function(snapshot){
        snapshot.forEach(function(childSnapshot){
            scoredata.name.push(childSnapshot.key);
            scoredata.score.push(childSnapshot.val().score);
            num++;
        });
    }).then(function(){
        game.add.text(240,50,'Rank',{fill: '#FFFFFF', fontSize: '60px'});
        if(num>=5){
            game.add.text(50,150,'1.' + scoredata.name[num-1] + ' score: ' + scoredata.score[num-1],{fill: '#FFFFFF', fontSize: '40px'});
            game.add.text(50,250,'2.' + scoredata.name[num-2] + ' score: ' + scoredata.score[num-2],{fill: '#FFFFFF', fontSize: '40px'});
            game.add.text(50,350,'3.' + scoredata.name[num-3] + ' score: ' + scoredata.score[num-3],{fill: '#FFFFFF', fontSize: '40px'});
            game.add.text(50,450,'4.' + scoredata.name[num-4] + ' score: ' + scoredata.score[num-4],{fill: '#FFFFFF', fontSize: '40px'});
            game.add.text(50,550,'5.' + scoredata.name[num-5] + ' score: ' + scoredata.score[num-5],{fill: '#FFFFFF', fontSize: '40px'});
        }
        else{
            if(num>=1) game.add.text(50,150,'1.' + scoredata.name[num-1] + ' score: ' + scoredata.score[num-1],{fill: '#FFFFFF', fontSize: '40px'});
            if(num>=2) game.add.text(50,250,'2.' + scoredata.name[num-2] + ' score: ' + scoredata.score[num-2],{fill: '#FFFFFF', fontSize: '40px'});
            if(num>=3) game.add.text(50,350,'3.' + scoredata.name[num-3] + ' score: ' + scoredata.score[num-3],{fill: '#FFFFFF', fontSize: '40px'});
            if(num>=4) game.add.text(50,450,'4.' + scoredata.name[num-4] + ' score: ' + scoredata.score[num-4],{fill: '#FFFFFF', fontSize: '40px'});
        }
    });
}

var rank = {
    create: function(){
        game.add.image(0, 0, 'bg');
        game.add.image(0, 0, 'ceiling');
        game.add.image(400, 0, 'ceiling');

        this.button = game.add.button(160,700,'menu',MenuOnClick,this,2,1,0);
    }
}

var nameSetting = {
    create: function(){
        game.add.image(0, 0, 'bg');
        game.add.image(0, 0, 'ceiling');
        game.add.image(400, 0, 'ceiling');
        game.add.text(80,300,'Please enter your name',{fill: '#FFFFFF', fontSize: '40px'});

        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER
        });
    },

    update: function(){
        inputhandle();
    }
}

function inputhandle(){
    var inputname = document.getElementById('inputname');
    inputname.style.visibility = 'visible';

    if(keyboard.enter.isDown && inputname.value != ''){
        name_now = inputname.value;
        inputname.style.visibility = 'hidden';
        inputname.value = '';
        game.state.start('maingame');
    }
}

var maingame = {
    preload: function(){
        game.load.image('bg', 'assets/bg.png');
        game.load.image('ice_floor', 'assets/ice_floor.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('wall', 'assets/wall.png');
        game.load.image('thick_ceiling', 'assets/thick_ceiling.png');
        game.load.spritesheet('player', 'assets/MARIO.png', 32, 54);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.audio('spring','assets/spring.wav');
        game.load.audio('damage','assets/damage.wav');
        game.load.audio('die','assets/die.wav');
    },

    create: function(){
        this.bg_music = game.add.audio('bg_music');
        this.bg_music.loop = true;
        this.bg_music.play();

        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.add.image(0, 0, 'bg');
        this.left_ceiling = game.add.sprite(0, 0, 'ceiling');
        this.right_ceiling = game.add.sprite(400, 0, 'ceiling');
        this.thick_ceiling = game.add.sprite(0, -1024, 'thick_ceiling');
        game.physics.arcade.enable(this.left_ceiling);
        game.physics.arcade.enable(this.right_ceiling);
        game.physics.arcade.enable(this.thick_ceiling);
        this.left_ceiling.body.checkCollision.down = false;
        this.right_ceiling.body.checkCollision.down = false;
        this.thick_ceiling.body.checkCollision.down = false;
        this.cursor = game.input.keyboard.createCursorKeys();

        //create player
        this.player = game.add.sprite(300, 0, 'player');
        this.player.animations.add('rightwalk', [1, 2], 8, true);
        this.player.animations.add('leftwalk', [3, 4], 8, true);
        this.player.animations.add('rightjump', [5, 6], 8, true);
        this.player.animations.add('leftjump', [7, 8], 8, true);
        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 900;
        this.player.touchfloor = undefined;
        this.player.life = 10;
        this.player.level = 0;
        this.player.unbeatableTime = 0;

        //create floor group
        this.floors = game.add.group();

        //create wall
        this.leftwall = game.add.sprite(-40, 0, 'wall');
        this.rightwall = game.add.sprite(583, 0, 'wall');
        game.physics.arcade.enable(this.leftwall);
        game.physics.arcade.enable(this.rightwall);
        this.leftwall.body.immovable = true;
        this.rightwall.body.immovable = true;

        //create scoreboard
        this.showlife = game.add.text(40,20,'',{fill: '#BB5500', fontSize: '40px'});
        this.score = game.add.text(380,20,'',{fill: '#BB5500', fontSize: '40px'});

        //create sound
        springSound = game.add.audio('spring');
        damageSound = game.add.audio('damage');
        dieSound = game.add.audio('die');
    },

    update: function(){
        game.physics.arcade.collide(this.player, this.floors, floor_effect);
        game.physics.arcade.collide(this.player, this.leftwall);
        game.physics.arcade.collide(this.player, this.rightwall);

        //update floor speed
        if(this.player.level <20){
            this.floors.setAll('body.velocity.y',-200);
            this.player.body.gravity.y = 900;
            floortimeInterval = 1000;
        }else if(this.player.level <40){
            this.floors.setAll('body.velocity.y',-300);
            this.player.body.gravity.y = 1050;
            floortimeInterval = 800;
        }else if(this.player.level <60){
            this.floors.setAll('body.velocity.y',-400);
            this.player.body.gravity.y = 1200;
            floortimeInterval = 600;
        }else{
            this.floors.setAll('body.velocity.y',-400);
            this.player.body.gravity.y = 1200;
            floortimeInterval = 600;
            this.left_ceiling.body.velocity.y = 20;
            this.right_ceiling.body.velocity.y = 20;
            this.thick_ceiling.body.velocity.y = 20;
        }

        //update scoreboard and lifetext
        this.showlife.setText('life: ' + this.player.life);
        this.score.setText('score: ' + this.player.level);
  
        //player control
        this.player.body.velocity.x = 0;
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -320;
            this.player.animations.play('leftwalk');
        }
        else if (this.cursor.right.isDown) {
            this.player.body.velocity.x = 320;
            this.player.animations.play('rightwalk');
        }
        else {
            this.player.animations.stop();
            this.player.frame = 0;
        }

        //generate floors
        if(game.time.now > lastTime + floortimeInterval) {
            lastTime = game.time.now;
            this.player.level +=1;
            final_score = this.player.level;
            var x = Math.random()*450;
            var rand = Math.random()*130;
            if(rand<20){
                this.floor = game.add.sprite(x, 900, 'ice_floor', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.scale.setTo(0.4,0.5);
            }
            else if(rand<30){
                this.floor = game.add.sprite(x, 900, 'trampoline', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                this.floor.scale.setTo(1.5,1.5);
                this.floor.frame = 3;
            }
            else if(rand<50){
                this.floor = game.add.sprite(x, 900, 'conveyorLeft', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.animations.add('scroll', [0, 1, 2, 3], 16, true);
                this.floor.play('scroll');
                this.floor.scale.setTo(1.5,1.5);
            }
            else if(rand<70){
                this.floor = game.add.sprite(x, 900, 'conveyorRight', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.animations.add('scroll', [0, 1, 2, 3], 16, true);
                this.floor.play('scroll');
                this.floor.scale.setTo(1.5,1.5);
            }
            else if(rand<90){
                this.floor = game.add.sprite(x, 900, 'fake', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
                this.floor.scale.setTo(1.5,1.5);
            }
            else if(rand<110){
                this.floor = game.add.sprite(x, 900, 'nails', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.scale.setTo(1.5,1.5);
                this.floor.body.setSize(96, 15, 0, 15);
            }
            else if(rand<130){
                this.floor = game.add.sprite(x, 900, 'normal', 0, this.floors);
                game.physics.arcade.enable(this.floor);
                this.floor.scale.setTo(0.2,0.2);
            }
            
            this.floor.body.immovable = true;
            this.floor.body.checkCollision.down = false;
            this.floor.body.checkCollision.left = false;
            this.floor.body.checkCollision.right = false;
        }

        //touching ceiling check
        if(this.player.body.y - this.left_ceiling.body.y < 0) {
            if(this.player.body.y  - this.left_ceiling.body.y < -40){
                this.player.body.y =  this.left_ceiling.body.y;
            }
            if(this.player.body.velocity.y < 0) {
                this.player.body.velocity.y = 0;
                this.player.body.y = 0;
            }
            if(game.time.now > this.player.unbeatableTime) {
                damageSound.play();
                this.player.life -= 3;
                game.camera.flash(0xff0000, 100);
                this.player.unbeatableTime = game.time.now + 3000;
            }
        }

        //gameover check
        if(this.player.life <= 0 || this.player.body.y > 900){
            if(this.player.body.y > 900) dieSound.play();
            this.bg_music.stop();
            game.state.start('gameover');
        }
        
    }
};

//floor collide callback function
function floor_effect(player,floor_now){
    if(floor_now.key=='normal'){
        if(player.touchfloor !== floor_now){
            game.camera.shake(0.1, 1500);
            player.touchfloor = floor_now;
        }
    }
    else if(floor_now.key=='ice_floor'){
        if(player.touchfloor !== floor_now){
            player.life += 1;
            player.touchfloor = floor_now;
        }
    }
    else if(floor_now.key=='trampoline'){
        springSound.play();
        floor_now.animations.play('jump');
        if(player.level<20){
            player.body.velocity.y = -500;
        }else if(player.level<40){
            player.body.velocity.y = -600;
        }else{
            player.body.velocity.y = -700;
        }
    }
    else if(floor_now.key=='conveyorLeft'){
        player.body.x -= 2;
    }
    else if(floor_now.key=='conveyorRight'){
        player.body.x += 2;
    }
    else if(floor_now.key=='fake'){
        if(player.touchfloor !== floor_now){
            floor_now.animations.play('turn');
            setTimeout(function() {
                floor_now.body.checkCollision.up = false;
            }, 100);
            player.touchfloor = floor_now;
        }
    }
    else if(floor_now.key=='nails'){
        if(player.touchfloor !== floor_now){
            damageSound.play();
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.touchfloor = floor_now;
        }
    }
}

var gameover = {
    preload: function(){
        game.load.image('gameover', 'assets/gameover.png');
    },

    create: function(){
        game.add.image(0, 0, 'bg');
        game.add.image(0, 0, 'ceiling');
        game.add.image(400, 0, 'ceiling');
        game.add.image(170,300,'gameover');

        uploadscore();

        keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER
        });

        this.button = game.add.button(160,700,'menu',MenuOnClick,this,2,1,0);
        game.add.text(30,100,'You score: ' + final_score,{fill: '#FFFFFF', fontSize: '80px'});
        game.add.text(80,570,'Press Enter to try again',{fill: '#FFFFFF', fontSize: '40px'});
    },

    update: function(){
        if(keyboard.enter.isDown){
            game.state.start('maingame');
        }
    }
}

function MenuOnClick(){
    game.state.start('menu');
}

function uploadscore(){
    firebase.database().ref('rank/' + name_now).set({
        score: final_score
    });
}

var game = new Phaser.Game(600, 900, Phaser.AUTO, 'gameDiv');
game.state.add('maingame', maingame);
game.state.add('menu',menu);
game.state.add('rank',rank);
game.state.add('nameSetting',nameSetting);
game.state.add('gameover',gameover);
game.state.start('menu');